<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\CongTy;

class CongTyController
{
    public function view_all(Request $rq)
    {
        $tim_kiem = $rq->tim_kiem;
        $title = 'Xem tất cả công ty';
        $array_cong_ty = CongTy::where('ten','like',"%$tim_kiem%")->paginate(3);

        return view('cong_ty.view_all',[
            'array_cong_ty' => $array_cong_ty,
            'title'         => $title,
            'tim_kiem'      => $tim_kiem,
        ]);
    }
    public function view_insert()
    {
    	return view('cong_ty.view_insert');
    }
    public function process_insert(Request $rq)
    {
    	CongTy::create($rq->all());

    	return redirect()->route('cong_ty.view_all')->with('sucess','Đã thêm thành công');
    }
    public function view_update($ma)
    {
    	$cong_ty = CongTy::find($ma);

    	return view('cong_ty.view_update',['cong_ty' => $cong_ty]);
    }
    public function process_update($ma,Request $rq)
    {
    	CongTy::find($ma)->update($rq->all());

    	return redirect()->route('cong_ty.view_all')->with('sucess','Đã sửa thành công');
    }
    public function delete($ma)
    {
    	CongTy::find($ma)->delete();

    	return redirect()->route('cong_ty.view_all')->with('sucess','Đã xoá thành công');
    }
    public function view_nhan_vien_cua_cong_ty($ma)
    {
        $array_nhan_vien = CongTy::with('array_nhan_vien')->find($ma);

        return $array_nhan_vien;
    }
}
