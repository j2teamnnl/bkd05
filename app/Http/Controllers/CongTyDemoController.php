<?php

namespace App\Http\Controllers;

use App\Models\CongTy;
use Illuminate\Http\Request;

class CongTyDemoController extends Controller
{
    protected $folder = 'cong_ty_demo';
    public function index()
    {
        $array_cong_ty = CongTy::all();

        return view("$this->folder.index",['array_cong_ty' => $array_cong_ty]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CongTy  $congTy
     * @return \Illuminate\Http\Response
     */
    public function show(CongTy $congTy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CongTy  $congTy
     * @return \Illuminate\Http\Response
     */
    public function edit(CongTy $cong_ty)
    {
        return view('cong_ty.view_update',['cong_ty' => $cong_ty]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CongTy  $congTy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CongTy $cong_ty)
    {
        $cong_ty->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CongTy  $congTy
     * @return \Illuminate\Http\Response
     */
    public function destroy(CongTy $cong_ty)
    {
        $cong_ty->delete();
    }
}
