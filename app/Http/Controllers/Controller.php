<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\Lop;

class Controller
{
    public function master()
    {
        return view('layout.master');
    }
    public function welcome()
    {
    	return view('welcome');
    }
    public function test()
    {
        return view('test');
    }
    public function upload(Request $rq)
    {
        $rq->file_anh->store('public/image');
    }
    public function show()
    {
        return view('show');
    }
}
