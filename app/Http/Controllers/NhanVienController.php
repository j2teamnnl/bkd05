<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\NhanVien;
use App\Models\CongTy;
use App\Http\Requests\NhanVienRequest;
use App\Notifications\NewStudent;
use App\Imports\NhanVienImport;
use Maatwebsite\Excel\Facades\Excel;

class NhanVienController
{
    public function view_all()
    {
        $array_nhan_vien = NhanVien::with('cong_ty')->get();


        return view('nhan_vien.view_all',[
        	'array_nhan_vien' => $array_nhan_vien,
        ]);
    }
    public function view_insert()
    {
        $array_cong_ty = CongTy::all();
    	return view('nhan_vien.view_insert',['array_cong_ty' => $array_cong_ty]);
    }
    public function process_insert(Request $rq)
    {
    	$nhan_vien = NhanVien::create($rq->all());
        $nhan_vien->notify(new NewStudent($nhan_vien));

    	// return redirect()->route('nhan_vien.view_all')->with('sucess','Đã thêm thành công');
    }
    public function view_update($ma)
    {
    	$nhan_vien = NhanVien::find($ma);

    	return view('nhan_vien.view_update',['nhan_vien' => $nhan_vien]);
    }
    public function process_update($ma,Request $rq)
    {
    	NhanVien::find($ma)->update($rq->all());

    	return redirect()->route('nhan_vien.view_all')->with('sucess','Đã sửa thành công');
    }
    public function delete($ma)
    {
    	NhanVien::find($ma)->delete();

    	return redirect()->route('nhan_vien.view_all')->with('sucess','Đã xoá thành công');
    }
    public function view_import_excel()
    {
        return view('nhan_vien.view_import_excel');
    }
    public function process_import_excel(Request $rq)
    {
        Excel::import(new NhanVienImport, $rq->file_excel);
    }
}
