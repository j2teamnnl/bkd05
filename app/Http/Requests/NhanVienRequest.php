<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NhanVienRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ten' => 'required',
            'ngay_sinh' => 'required|date',
            'email' => 'required|email|unique:nhan_vien,email',
            'gioi_tinh' => 'required|boolean',
            'ma_cong_ty' => 'required|exists:cong_ty,ma',
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute bắt buộc phải điền',
            'email' => ':attribute phải hợp lệ',
            'date' => ':attribute phải hợp lệ',
        ];
    }
    public function attributes()
    {
        return [
            'ten' => 'Tên',
            'ngay_sinh' => 'Ngày sinh',
            'email' => 'Email',
            'gioi_tinh' => 'Giới tính',
            'ma_cong_ty' => 'Công ty'
        ];
    }
}
