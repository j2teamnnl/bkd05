<?php

namespace App\Imports;

use App\Models\NhanVien;
use App\Models\CongTy;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Exception;

class NhanVienImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $array = array_filter($row);
        if(!empty($array)){
            $array = [
                'ten' => $row['ten'],
                'gioi_tinh' => ($row['gioi_tinh']=='Nam') ? 1 : 0,
                'ngay_sinh' => date_format(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['ngay_sinh']),'Y-m-d'),
                'ma_cong_ty' => CongTy::firstOrCreate(['ten' => $row['ten_cong_ty']])->ma
            ];
            
            return new NhanVien($array);
        }
    }
}
