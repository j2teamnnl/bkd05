<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CongTy extends Model
{
    protected $table = 'cong_ty';
    protected $fillable = ['ten','sdt','email','dia_chi'];
    protected $primaryKey = 'ma';

    public $timestamps = false;
    public function array_nhan_vien()
    {
    	return $this->hasMany('App\Models\NhanVien', 'ma_cong_ty', 'ma');
    }
}
