<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Diem extends Model
{
    use HasCompositePrimaryKey;
    protected $table = 'diem';

    public $timestamps = false;
    protected $fillable = [
    	'ma_mon',
        'ma_sinh_vien',
        'diem',
	];
    protected $primaryKey = ['ma_mon','ma_sinh_vien'];
    public $incrementing = false;
}
