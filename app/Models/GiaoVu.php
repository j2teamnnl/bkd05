<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GiaoVu extends Model
{
    protected $table = 'giao_vu';
    public $timestamps = false;
    protected $fillable = ['ten','email','mat_khau'];
    protected $primaryKey = 'ma';
}
