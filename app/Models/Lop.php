<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lop extends Model
{
    protected $table = 'lop';
    protected $fillable = ['ten'];
    protected $primaryKey = 'ma';

    public $timestamps = false;
}
