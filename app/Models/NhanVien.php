<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class NhanVien extends Model
{
    use Notifiable;
    protected $table = 'nhan_vien';
    protected $fillable = [
    	'ten',
    	'ngay_sinh',
    	'email',
        'gioi_tinh',
    	'ma_cong_ty'
    ];
    protected $primaryKey = 'ma';

    public $timestamps = false;
    public function cong_ty()
    {
    	return $this->belongsTo('App\Models\CongTy','ma_cong_ty');
    }
}
