<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Lop::class, function (Faker $faker) {
    return [
        'ten' => $faker->cityPrefix
    ];
});
