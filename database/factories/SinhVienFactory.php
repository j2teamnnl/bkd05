<?php

$factory->define(App\Models\SinhVien::class, function () {
	$faker = Faker\Factory::create('vi_VN'); 
    return [
        'ho' => $faker->firstName,
    	'ten' => $faker->lastName,
    	'ngay_sinh' => $faker->dateTimeBetween('-30 years', '-18 years'),
    	'gioi_tinh' => $faker->boolean,
    	'dia_chi' => $faker->address,
    	'ma_lop' => App\Models\Lop::inRandomOrder()->value('ma'),
    ];
});
