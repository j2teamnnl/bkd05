<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NhanVien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nhan_vien', function (Blueprint $table) {
           $table->increments('ma');
           $table->string('ten',50);
           $table->date('ngay_sinh');
           $table->boolean('gioi_tinh');
           $table->integer('ma_cong_ty')->unsigned();
           $table->foreign('ma_cong_ty')->references('ma')->on('cong_ty');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
