<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SinhVien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sinh_vien', function (Blueprint $table) {
           $table->increments('ma');
           $table->string('ho',50);
           $table->string('ten',50);
           $table->date('ngay_sinh');
           $table->boolean('gioi_tinh');
           $table->integer('ma_lop')->unsigned();
           $table->foreign('ma_lop')->references('ma')->on('lop');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sinh_vien');
    }
}
