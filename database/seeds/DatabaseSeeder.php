<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(LopSeeder::class);
        // $this->call(SinhVienSeeder::class);
        // $this->call(MonSeeder::class);
        $this->call(DiemSeeder::class);
    }
}
