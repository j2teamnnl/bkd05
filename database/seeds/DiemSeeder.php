<?php

use Illuminate\Database\Seeder;

use App\Models\Mon;
use App\Models\SinhVien;
use App\Models\Diem;

class DiemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array_sinh_vien = SinhVien::inRandomOrder()->limit('250')->get();
        $array_mon = Mon::inRandomOrder()->limit('15')->get();
        foreach ($array_sinh_vien as $sinh_vien) {
        	foreach ($array_mon as $mon) {
        		Diem::insert([
        			'ma_mon' => $mon->ma,
        			'ma_sinh_vien' => $sinh_vien->ma,
        			'diem' => rand(1,10),
        		]);
        	}
        }
    }
}
