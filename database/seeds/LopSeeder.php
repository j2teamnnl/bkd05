<?php

use Illuminate\Database\Seeder;

class LopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Lop::class,10)->create();
    }
}
