<?php

use Illuminate\Database\Seeder;

class MonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Mon::class,20)->create();
    }
}
