@extends('layout.master')

@section('content')
<a href="{{ route('cong_ty.view_insert') }}">
	Thêm
</a>
@if (Session::has('sucess'))
	<h1>
		{{ Session::get('sucess') }}
	</h1>
@endif
<table class="table table-hover">
	<tr>
		<th>
			Tên
		</th>
		<th>
			SDT
		</th>
		<th>
			Email
		</th>
		<th>
			Địa chỉ
		</th>
		<th>
			Xem
		</th>
		@if (Session::get('cap_do')==1)
			<th>
				Sửa
			</th>
			<th>
				Xoá
			</th>
		@endif
	</tr>
	@foreach ($array_cong_ty as $cong_ty)
		<tr>
			<td>
				{{ $cong_ty->ten }}
			</td>
			<td>
				{{ $cong_ty->sdt }}
			</td>
			<td>
				{{ $cong_ty->email }}
			</td>
			<td>
				{{ $cong_ty->dia_chi }}
			</td>
			<td>
				<a href="{{ route('cong_ty.view_nhan_vien_cua_cong_ty',['ma' => $cong_ty->ma]) }}">
					Xem nhân viên của công ty
				</a>
			</td>
			@if (Session::get('cap_do')==1)
				<td>
					<a href="{{ route('cong_ty.view_update',['ma' => $cong_ty->ma]) }}">
						Sửa
					</a>
				</td>
				<td>
					<a href="{{ route('cong_ty.delete',['ma' => $cong_ty->ma]) }}">
						Xoá
					</a>
				</td>
			@endif
		</tr>
	@endforeach
</table>
{{$array_cong_ty->appends(['tim_kiem' => $tim_kiem])->links()}}
@endsection