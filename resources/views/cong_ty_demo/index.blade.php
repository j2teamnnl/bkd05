<a href="{{ route('cong_ty.create') }}">
	Thêm
</a>
@if (Session::has('sucess'))
	<h1>
		{{ Session::get('sucess') }}
	</h1>
@endif
<table border="1" width="100%">
	<tr>
		<th>
			Tên
		</th>
		<th>
			SDT
		</th>
		<th>
			Email
		</th>
		<th>
			Địa chỉ
		</th>
		<th>
			Xem
		</th>
		<th>
			Sửa
		</th>
		<th>
			Xoá
		</th>
	</tr>
	@foreach ($array_cong_ty as $cong_ty)
		<tr>
			<td>
				{{ $cong_ty->ten }}
			</td>
			<td>
				{{ $cong_ty->sdt }}
			</td>
			<td>
				{{ $cong_ty->email }}
			</td>
			<td>
				{{ $cong_ty->dia_chi }}
			</td>
			<td>
				<a href="{{ route('cong_ty.show',['ma' => $cong_ty->ma]) }}">
					Xem nhân viên của công ty
				</a>
			</td>
			<td>
				<a href="{{ route('cong_ty.edit',['ma' => $cong_ty->ma]) }}">
					Sửa
				</a>
			</td>
			<td>
				<a href="{{ route('cong_ty.destroy',['ma' => $cong_ty->ma]) }}">
					Xoá
				</a>
			</td>
		</tr>
	@endforeach
</table>