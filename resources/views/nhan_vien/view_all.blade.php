<a href="{{ route('nhan_vien.view_insert') }}">
	Thêm
</a>
<br>
<a href="{{ route('nhan_vien.view_import_excel') }}">
	Thêm bằng Excel
</a>
@if (Session::has('sucess'))
	<h1>
		{{ Session::get('sucess') }}
	</h1>
@endif
<table border="1" width="100%">
	<tr>
		<th>
			Tên
		</th>
		<th>
			Giới tính
		</th>
		<th>
			Ngày sinh
		</th>
		<th>
			Tên Công Ty
		</th>
		<th>
			Sửa
		</th>
		<th>
			Xoá
		</th>
	</tr>
	@foreach ($array_nhan_vien as $nhan_vien)
		<tr>
			<td>
				{{ $nhan_vien->ten }}
			</td>
			<td>
				{{ $nhan_vien->gioi_tinh }}
			</td>
			<td>
				{{ $nhan_vien->ngay_sinh }}
			</td>
			<td>
				{{ $nhan_vien->cong_ty->ten }}
			</td>
			<td>
				<a href="{{ route('nhan_vien.view_update',['ma' => $nhan_vien->ma]) }}">
					Sửa
				</a>
			</td>
			<td>
				<a href="{{ route('nhan_vien.delete',['ma' => $nhan_vien->ma]) }}">
					Xoá
				</a>
			</td>
		</tr>
	@endforeach
</table>