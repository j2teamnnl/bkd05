<form action="{{ route('nhan_vien.process_insert') }}" method="post">
	{{ csrf_field() }}
	Tên
	<input type="text" name="ten" value="{{ old('ten') }}">
	{{ $errors->first('ten') }}
	<br>
	Ngày sinh
	<input type="date" name="ngay_sinh" value="{{ old('ngay_sinh') }}">
	{{ $errors->first('ngay_sinh') }}
	<br>
	Email
	<input type="text" name="email" value="{{ old('email') }}">
	{{ $errors->first('email') }}
	<br>
	Giới tính
	<input type="radio" name="gioi_tinh" value="1"
	@if (old('gioi_tinh')==1)
		checked 
	@endif>Nam
	<input type="radio" name="gioi_tinh" value="0"
	@if (old('gioi_tinh')==='0')
		checked 
	@endif>Nữ
	{{ $errors->first('gioi_tinh') }}
	<br>
	Công ty
	<select name="ma_cong_ty">
		@foreach ($array_cong_ty as $cong_ty)
			<option value="{{ $cong_ty->ma }}" 
			@if (old('ma_cong_ty')==$cong_ty->ma)
				selected 
			@endif>
				{{ $cong_ty->ten }}
			</option>
		@endforeach
	</select>
	{{ $errors->first('ma_cong_ty') }}
	<br>
	<button>Thêm</button>
</form>