<?php

Route::get('show','Controller@show');
Route::get('test','Controller@test');
Route::post('upload','Controller@upload')->name('upload');

Route::get("view_login","LoginController@view_login")->name('view_login');
Route::post("process_login","LoginController@process_login")->name('process_login');

Route::group(['middleware' => 'CheckGiaoVu'],function(){
	Route::get("welcome","Controller@welcome")->name('welcome');
	Route::get("logout","LoginController@logout")->name('logout');

	$controller = "CongTyController";
	Route::group(["prefix" => "cong_ty", 'as' => 'cong_ty.'],function() use ($controller){
		Route::get("","$controller@view_all")->name("view_all");
		Route::group(['middleware' => 'CheckSuperAdmin'],function() use ($controller){
			Route::get("view_insert","$controller@view_insert")->name("view_insert");
			Route::post("process_insert","$controller@process_insert")->name("process_insert");
			Route::get("view_update/{ma}","$controller@view_update")->name("view_update");
			Route::post("process_update/{ma}","$controller@process_update")->name("process_update");
			Route::get("delete/{ma}","$controller@delete")->name("delete");

			// xem nhân viên của công ty
			Route::get("view_nhan_vien_cua_cong_ty/{ma}","$controller@view_nhan_vien_cua_cong_ty")->name("view_nhan_vien_cua_cong_ty");
		});
	});

	$controller = "NhanVienController";
	Route::group(["prefix" => "nhan_vien", 'as' => 'nhan_vien.'],function() use ($controller){
		Route::get("","$controller@view_all")->name("view_all");
		Route::get("view_insert","$controller@view_insert")->name("view_insert");
		Route::post("process_insert","$controller@process_insert")->name("process_insert");
		Route::get("view_update/{ma}","$controller@view_update")->name("view_update");
		Route::post("process_update/{ma}","$controller@process_update")->name("process_update");
		Route::get("delete/{ma}","$controller@delete")->name("delete");

		Route::get("view_import_excel","$controller@view_import_excel")->name("view_import_excel");
		Route::post("process_import_excel","$controller@process_import_excel")->name("process_import_excel");
	});

	Route::resource('cong_ty_demo','CongTyDemoController');
});

